-- Allow throwing some default objects
--

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

throwing.register_bow(modname..":bow_wood", {
	itemcraft = "default:wood",
	description = "Wooden Bow",
	texture = 'default_torch_on_floor.png',
	throw_itself = true
})
