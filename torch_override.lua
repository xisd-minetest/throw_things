-- Allow throwing some default objects
--

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

local throw_key = 'sneak'
--~ jump, right, left, LMB, RMB, sneak, aux1, down, up} 
local creative_mode = minetest.settings:get_bool("creative_mode")

local is_creative = function(name)
	return creative_mode or minetest.check_player_privs(name, {creative = true})
end
local arrow_torch = modname..":arrow_torch"
--~ local torch_previous_on_use = minetest.registered_items["default:torch"].on_use

--
-- ---[ Throw arrow functions ]---
-- ------------------------------

local throw_arrow = function(player,arrow)
	local VELOCITY = 19
	local GRAVITY = -9.81
	
	-- Get pos and direction
	local pos = player:getpos()
	local dir = player:get_look_dir()
	pos.y = pos.y + 1.5
	dir.y = dir.y + 0.2
	
	-- Get pos and direction
	local obj = (minetest.registered_items[arrow].spawn_arrow_entity or throwing.spawn_arrow_entity)(pos, arrow, player)
	
	local luaentity = obj:get_luaentity()
	luaentity.player = player:get_player_name()
	if not luaentity.item then
		luaentity.item = arrow
	end
	
	--~ obj:get_luaentity():set_node({ name = "default:torch" })
	obj:setvelocity(vector.multiply(dir, VELOCITY))
	obj:setacceleration({x=dir.x*-3, y=GRAVITY, z=dir.z*-3})
end


local throw_torch_on_drop = function(itemstack, player, pos)
		
	-- If sneak key is pressed, throw the thing
	if  player and player:get_player_control()[throw_key] then
		throw_arrow(player,arrow_torch)
		local name = player:get_player_name()
		if not is_creative(name) then itemstack:take_item() end
		return itemstack		
	-- Else drop it
	else
		--~ minetest.handle_node_drops(pos, itemstack)
        itemstack = minetest.item_drop(itemstack, player, pos)
		return itemstack
	end
end


--
-- ---[ default:torch ]---
-- ------------------------------

local torch = "default:torch"


throwing.register_arrow(arrow_torch, {
	description = "Broken Throwned Torch",
	tiles = {'default_torch_on_floor.png'},
	on_hit_sound = "throwing_arrow",
	drop = "default:torch",
	on_hit = function(self, pos, last_pos, node, object, hitter, data)
		if last_pos and node then
			last_pos = vector.round(last_pos)
			local old_node = minetest.get_node(last_pos).name
			local placed = minetest.place_node(last_pos,{name=torch})
	
			-- FIXME: minetest.place_node returns true even when the node was not placed
			-- FUZZY: drop item if node at last pos as changed and node under is not torch
			--~ if not placed then
			--~ print("------- last pos was : ".. minetest.pos_to_string(last_pos))
			--~ print("------- It was : ".. old_node)
			
			--~ minetest.after(1, function(last_pos, old_node) 
				local new_node = minetest.get_node(last_pos).name
				print("------- Now it is : ".. new_node)
				if new_node == old_node and minetest.get_node(vector.add(last_pos, {x=0,y=-1,z=0})).name ~= torch then
					-- If node placing failed, drop the item
					local obj = minetest.add_item(last_pos, torch)
					local luaentity = obj:get_luaentity()

					luaentity.on_step = function(self, dtime)
						local pos = self.object:get_pos()
						
						if minetest.get_node(pos).name == torch then return end

						local under = vector.add(pos,{x=0,y=-1,z=0})
						if minetest.get_node(under).name ~= 'air' then
							minetest.place_node(pos,{name=torch})
						end
						
						if minetest.get_node(pos).name == torch then self.object:remove() end
					end
					
				end	
			--~ end, last_pos, old_node)
			--~ local dir = vector.direction(pos, last_pos)		
			--~ local wall_or_floor = minetest.get_node(vector.add(last_pos, dir)) 
			--~ if wall_or_floor ~= "air" then
				--~ local p2 = minetest.dir_to_facedir(dir)
				--~ minetest.set_node(last_pos,{name="default:torch", param2=p2})
			--~ end
			print(dump(placed))
		else
			object:punch(hitter, 1, {
				full_punch_interval = 1,
				damage_groups = {fleshy = 2, choppy=2, snappy=3, flammable = 10}
			})
		end
	end
})

if minetest.registered_nodes[arrow_torch] then
	minetest.override_item(arrow_torch, {
		drawtype = "mesh",
		mesh = "torch_floor.obj",
		paramtype2 = "wallmounted",
		drop = "default:torch",
		selection_box = {
			type = "wallmounted",
			wall_bottom = {-1/8, -1/2, -1/8, 1/8, 2/16, 1/8},
		}
	})
end

minetest.register_craft({
    output = "default:torch",
    type = "shapeless",
    recipe = { arrow_torch },
})

if minetest.registered_entities[arrow_torch] then
	minetest.registered_entities[arrow_torch].visual_size = {x = 0.9, y = 0.9}
end

minetest.override_item("default:torch", {
   on_drop = throw_torch_on_drop,
})



	

